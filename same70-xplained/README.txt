README
======

This README file discusses the port of NuttX to the Atmel SAM E70 Xplained
Evaluation Kit (ATSAME70-XPLD).  This board features the ATSAME70Q21 Cortex-M7
microcontroller.

Contents
========

  - Status/Open Issues
  - Serial Console
  - SD card
  - Automounter
  - LEDs and Buttons
  - AT24MAC402 Serial EEPROM
  - Program FLASH Access
  - Networking
  - USBHS Device Controller Driver
  - MCAN1 Loopback Test
  - SPI Slave
  - Debugging
  - Configurations

Status/Open Issues
==================

Although this configuration is not particualarly different from the
SAMV71-XULT board, my initial attempts to debug the board have not been
successful.  The code is just not behaving correctly in excuting the first
few instructions after reset.  I have very early realease boards and I am
suspected some tool/board issue:  It appears that I write the code
correctly to FLASH and the GPNVM is configured so that the FLASH lies at
address 0x00000000, but trying to step through with AtmelStudio 7 results
in uninterpretble behavior.  Using the Segger J-Link, I get errors trying
to reset and halt the board so I am unable to use that debugger either.

WARNING:  This README derives heavily from the SAMV71-XULT README file and
may still contain some logic that pertains only to that board.  This is a
work in progress

See also configs/samv71-xult/README.txt

Serial Console
==============

The SAME70-XPLD has no on-board RS-232 drivers so it will be necessary to
use either the VCOM or an external RS-232 driver.  Here are some options.

  - Arduino Serial Shield:  One option is to use an Arduino-compatible
    serial shield.  This will use the RXD and TXD signals available at pins
    0 an 1, respectively, of the Arduino "Digital Low" connector.  On the
    SAME70-XPLD board, this corresponds to UART3:

    ------ ------ ------- ------- --------
    Pin on SAME70 Arduino Arduino SAME70
    J503   PIO    Name    Pin     Function
    ------ ------ ------- ------- --------
      1    PD28   D0/RX0  0       URXD3
      2    PD30   D1/TX0  1       UTXD3
    ------ ------ ------- ------- --------

    In this configuration, an external RS232 driver can also be used
    instead of the shield.  Simply connext as follows:

    --------- -----------
    Arduino   RS-232
    Pin Label Connection
    --------- -----------
    D0 (RXD)  RX
    D1 (TXD)  TX
    GND       GND
    5VO       Vcc
    --------- -----------

  - Arduino Communications.  Additional UART/USART connections are available
    on the Arduino Communications connection J505 and J507:

    --------- ---------- --------------------------------
    Connector SAME70     Pin Description
    --------- ---------- --------------------------------
    J503  1   URXD3 PD28 Standard Arduino serial (D0/RXD)
    J503  2   UTXD3 PD30 Standard Arduino serial (D1/TXD)
    --------- ---------- --------------------------------
    J505  3   URXD4 PD18 Arduino D19
    J505  4   UTXD4 PD19 Arduino D18
    J505  5   RXD2  PD15 Arduino D17
    J505  6   TXD2  PD16 Arduino D16
    J505  7   RXD0  PB0  Arduino D15
    J505  8   TXD0  PB1  Arduino D14
    --------- ---------- --------------------------------
    J507 27   RXD1  PA21 Arduino D46
    J507 28   TXD1  PB4  Arduino D47
    --------- ---------- --------------------------------

  - SAMV7-XULT EXTn connectors.  USART pins are also available the EXTn
    connectors.  The following are labelled in the User Guide for USART
    functionality:

    SAME70 Xplained Connectors
    --------- ---------- --------------------------------
    Connector SAME70     Pin Description
    --------- ---------- --------------------------------
    J401 13   RXD0  PB0  EXT1 UART_RX
    J401 14   TXD0  PB1  EXT1 UART_7X
    --------- ---------- --------------------------------
    J402 13   RXD1  PA21 EXT2 UART_RX
    J402 14   TXD1  PB4  EXT2 UART_TX
    --------- ---------- --------------------------------

  - VCOM.  The Virtual Com Port gateway is available on USART1:

    EDBG VCOM Interface
    ---------------- --------- --------------------------
    EDBG Singal      SAME70
    ---------------- --------- --------------------------
    EDBG_CDC_UART_RX TXD1 PB4
    EDBG_CDC_UART_TX RXD1 PA21
    ---------------- --------- --------------------------

Any of these options can be selected as the serial console by:

  1. Enabling the UART/USART peripheral in the
     "System Type -> Peripheral Selection" menu, then
  2. Configuring the peripheral in the "Drivers -> Serial Configuration"
     menu.

SD Card
=======

Card Slot
---------
The SAM E70 Xplained has one standard SD card connector that is connected to
the High Speed Multimedia Card Interface (HSMCI) of the SAM
E70. SD card connector:

  ------ ----------------- ---------------------
  SAME70 SAME70            Shared functionality
  Pin    Function
  ------ ----------------- ---------------------
  PA30   MCDA0 (DAT0)
  PA31   MCDA1 (DAT1)
  PA26   MCDA2 (DAT2)
  PA27   MCDA3 (DAT3)      Camera
  PA25   MCCK (CLK)        Shield
  PA28   MCCDA (CMD)
  PD18   Card Detect (C/D) Shield
  ------ ----------------- ---------------------

Configuration Settings
----------------------
Enabling HSMCI support. The SAMV7-XULT provides a one, full-size SD memory
card slots.  The full size SD card slot connects via HSMCI0.  Support for
the SD slots can be enabled with the following settings:

  System Type->SAMV7 Peripheral Selection
    CONFIG_SAMV7_HSMCI0=y                 : To enable HSMCI0 support
    CONFIG_SAMV7_XDMAC=y                  : XDMAC is needed by HSMCI0/1

  System Type
    CONFIG_SAMV7_GPIO_IRQ=y               : PIO interrupts needed
    CONFIG_SAMV7_GPIOD_IRQ=y              : Card detect pin is on PD18

  Device Drivers -> MMC/SD Driver Support
    CONFIG_MMCSD=y                        : Enable MMC/SD support
    CONFIG_MMSCD_NSLOTS=1                 : One slot per driver instance
    CONFIG_MMCSD_MULTIBLOCK_DISABLE=y     : (REVISIT)
    CONFIG_MMCSD_HAVECARDDETECT=y         : Supports card-detect PIOs
    CONFIG_MMCSD_MMCSUPPORT=n             : Interferes with some SD cards
    CONFIG_MMCSD_SPI=n                    : No SPI-based MMC/SD support
    CONFIG_MMCSD_SDIO=y                   : SDIO-based MMC/SD support
    CONFIG_SDIO_DMA=y                     : Use SDIO DMA
    CONFIG_SDIO_BLOCKSETUP=y              : Needs to know block sizes

  RTOS Features -> Work Queue Support
    CONFIG_SCHED_WORKQUEUE=y              : Driver needs work queue support

  Application Configuration -> NSH Library
    CONFIG_NSH_ARCHINIT=y                 : NSH board-initialization, OR
    CONFIG_BOARD_INITIALIZE=y

Using the SD card
-----------------

1) After booting, the HSCMI device will appear as /dev/mmcsd0.

2) If you try mounting an SD card with nothing in the slot, the mount will
   fail:

     nsh> mount -t vfat /dev/mmcsd0 /mnt/sd0
     nsh: mount: mount failed: 19

   NSH can be configured to provide errors as strings instead of
   numbers.  But in this case, only the error number is reported.  The
   error numbers can be found in nuttx/include/errno.h:

     #define ENODEV              19
     #define ENODEV_STR          "No such device"

   So the mount command is saying that there is no device or, more
   correctly, that there is no card in the SD card slot.

3) Inserted the SD card.  Then the mount should succeed.

    nsh> mount -t vfat /dev/mmcsd0 /mnt/sd0
    nsh> ls /mnt/sd1
    /mnt/sd1:
     atest.txt
    nsh> cat /mnt/sd1/atest.txt
    This is a test

   NOTE:  See the next section entitled "Auto-Mounter" for another way
   to mount your SD card.

4) Before removing the card, you must umount the file system.  This is
   equivalent to "ejecting" or "safely removing" the card on Windows:  It
   flushes any cached data to an SD card and makes the SD card unavailable
   to the applications.

     nsh> umount -t /mnt/sd0

   It is now safe to remove the card.  NuttX provides into callbacks
   that can be used by an application to automatically unmount the
   volume when it is removed.  But those callbacks are not used in
   these configurations.

Auto-Mounter
============

  NuttX implements an auto-mounter than can make working with SD cards
  easier.  With the auto-mounter, the file system will be automatically
  mounted when the SD card is inserted into the HSMCI slot and automatically
  unmounted when the SD card is removed.

  Here is a sample configuration for the auto-mounter:

    File System Configuration
      CONFIG_FS_AUTOMOUNTER=y

    Board-Specific Options
      CONFIG_SAME70XPLAINED_HSMCI0_AUTOMOUNT=y
      CONFIG_SAME70XPLAINED_HSMCI0_AUTOMOUNT_FSTYPE="vfat"
      CONFIG_SAME70XPLAINED_HSMCI0_AUTOMOUNT_BLKDEV="/dev/mmcsd0"
      CONFIG_SAME70XPLAINED_HSMCI0_AUTOMOUNT_MOUNTPOINT="/mnt/sdcard"
      CONFIG_SAME70XPLAINED_HSMCI0_AUTOMOUNT_DDELAY=1000
      CONFIG_SAME70XPLAINED_HSMCI0_AUTOMOUNT_UDELAY=2000

  WARNING:  SD cards should never be removed without first unmounting
  them.  This is to avoid data and possible corruption of the file
  system.  Certainly this is the case if you are writing to the SD card
  at the time of the removal.  If you use the SD card for read-only access,
  however, then I cannot think of any reason why removing the card without
  mounting would be harmful.

LEDs and Buttons
================

LEDs
----
A single LED is available driven by PC8.

These LEDs are not used by the board port unless CONFIG_ARCH_LEDS is
defined.  In that case, the usage by the board port is defined in
include/board.h and src/sam_autoleds.c. The LEDs are used to encode
OS-related events as follows:

  ------------------- ----------------------- ------
  SYMBOL              Meaning                 LED
  ------------------- ----------------------- ------
  LED_STARTED         NuttX has been started  OFF
  LED_HEAPALLOCATE    Heap has been allocated OFF
  LED_IRQSENABLED     Interrupts enabled      OFF
  LED_STACKCREATED    Idle stack created      ON
  LED_INIRQ           In an interrupt         N/C
  LED_SIGNAL          In a signal handler     N/C
  LED_ASSERTION       An assertion failed     N/C
  LED_PANIC           The system has crashed  FLASH

Thus is LED is statically on, NuttX has successfully  booted and is,
apparently, running normally.  If LED is flashing at approximately
2Hz, then a fatal error has been detected and the system has halted.

Buttons
-------
SAM E70 Xplained contains two mechanical buttons. One button is the RESET
button connected to the SAM E70 reset line and the other, PA11, is a generic
user configurable button. When a button is pressed it will drive the I/O
line to GND.

NOTE: There are no pull-up resistors connected to the generic user buttons
so it is necessary to enable the internal pull-up in the SAM E70 to use the
button.

AT24MAC402 Serial EEPROM
========================

Ethernet MAC Address
--------------------
The SAM E70 Xplained features one external AT24MAC402 serial EEPROM with an
EIA-48 MAC address connected to the SAM E70 through I2C. This device
contains a MAC address for use with the Ethernet interface.

Connectivity:

  ------ -------- --------
  SAME70 SAME70   I2C
  Pin    Function Function
  ------ -------- --------
  PA03   TWID0    SDA
  PA04   TWICK0   SCL
  ------ -------- --------

I2C address:

  The 7-bit addresses of the AT24 part are 0b1010AAA for the normal 2Kbit
  memory and 0b1011aaa for the "extended memory" where aaa is the state of
  the A0, A1, and A3 pins on the part.  On the SAME70-XPLD board, these
  are all pulled high so the full, 7-bit address is 0x5f.

Configuration
-------------

  System Type -> SAMV7 Peripheral Support
    CONFIG_SAMV7_TWIHS0=y                : Used to access the EEPROM
    CONFIG_SAMV7_TWIHS0_FREQUENCY=100000

  Device drivers -> Memory Technology Devices
    CONFIG_MTD_AT24XX=y                  : Enable the AT24 device driver
    CONFIG_AT24XX_SIZE=2                 : Normal EEPROM is 2Kbit (256b)
    CONFIG_AT24XX_ADDR=0x57              : Normal EEPROM address */
    CONFIG_AT24XX_EXTENDED=y             : Supports an extended memory region
    CONFIG_AT24XX_EXTSIZE=160            : Extended address up to 0x9f

MTD Configuration Data
----------------------
The AT24 EEPROM can also be used to storage of up to 256 bytes of
configuration data:

  Device drivers -> Memory Technology Devices

The configuration data device will appear at /dev/config.

Networking
==========

KSZ8081RNACA Connections
------------------------

  ------ --------- ---------
  SAME70 SAME70    Ethernet
  Pin    Function  Functio
  ------ --------- ---------
  PD0    GTXCK     REF_CLK
  PD1    GTXEN     TXEN
  PD2    GTX0      TXD0
  PD3    GTX1      TXD1
  PD4    GRXDV     CRS_DV
  PD5    GRX0      RXD0
  PD6    GRX1      RXD1
  PD7    GRXER     RXER
  PD8    GMDC      MDC
  PD9    GMDIO     MDIO
  PA14   GPIO      INTERRUPT
  PC10   GPIO      RESET
  ------ --------- ---------

Selecting the GMAC peripheral
-----------------------------

  System Type -> SAMV7 Peripheral Support
    CONFIG_SAMV7_EMAC0=y                 : Enable the GMAC peripheral (aka, EMAC0)
    CONFIG_SAMV7_TWIHS0=y                : We will get the MAC address from the AT24 EEPROM
    CONFIG_SAMV7_TWIHS0_FREQUENCY=100000

  System Type -> EMAC device driver options
    CONFIG_SAMV7_EMAC0_NRXBUFFERS=16     : Set aside some RS and TX buffers
    CONFIG_SAMV7_EMAC0_NTXBUFFERS=8
    CONFIG_SAMV7_EMAC0_RMII=y            : The RMII interfaces is used on the board
    CONFIG_SAMV7_EMAC0_AUTONEG=y         : Use autonegotiation
    CONFIG_SAMV7_EMAC0_PHYADDR=1         : KSZ8061 PHY is at address 1
    CONFIG_SAMV7_EMAC0_PHYSR=30          : Address of PHY status register on KSZ8061
    CONFIG_SAMV7_EMAC0_PHYSR_ALTCONFIG=y : Needed for KSZ8061
    CONFIG_SAMV7_EMAC0_PHYSR_ALTMODE=0x7 : "    " " " "     "
    CONFIG_SAMV7_EMAC0_PHYSR_10HD=0x1    : "    " " " "     "
    CONFIG_SAMV7_EMAC0_PHYSR_100HD=0x2   : "    " " " "     "
    CONFIG_SAMV7_EMAC0_PHYSR_10FD=0x5    : "    " " " "     "
    CONFIG_SAMV7_EMAC0_PHYSR_100FD=0x6   : "    " " " "     "

  PHY selection.  Later in the configuration steps, you will need to select
  the KSZ8061 PHY for EMAC (See below)

  Networking Support
    CONFIG_NET=y                         : Enable Neworking
    CONFIG_NET_NOINTS=y                  : Use the work queue, not interrupts for processing
    CONFIG_NET_SOCKOPTS=y                : Enable socket operations
    CONFIG_NET_ETH_MTU=562               : Maximum packet size (MTU) 1518 is more standard
    CONFIG_NET_ETH_TCP_RECVWNDO=562      : Should be the same as CONFIG_NET_ETH_MTU
    CONFIG_NET_ARP=y                     : ARP support should be enabled
    CONFIG_NET_ARP_SEND=y                : Use ARP to get peer address before sending
    CONFIG_NET_TCP=y                     : Enable TCP/IP networking
    CONFIG_NET_TCPBACKLOG=y              : Support TCP/IP backlog
    CONFIG_NET_TCP_READAHEAD=y           : Enable TCP read-ahead buffering
    CONFIG_NET_TCP_WRITE_BUFFERS=y       : Enable TCP write buffering
    CONFIG_NET_UDP=y                     : Enable UDP networking
    CONFIG_NET_BROADCAST=y               : Support UDP broadcase packets
    CONFIG_NET_ICMP=y                    : Enable ICMP networking
    CONFIG_NET_ICMP_PING=y               : Needed for NSH ping command
                                         : Defaults should be okay for other options
  Device drivers -> Network Device/PHY Support
    CONFIG_NETDEVICES=y                  : Enabled PHY selection
    CONFIG_ETH0_PHY_KSZ8061=y            : Select the KSZ8061 PHY used with EMAC0

  Device drivers -> Memory Technology Devices
    CONFIG_MTD_AT24XX=y                  : Enable the AT24 device driver
    CONFIG_AT24XX_SIZE=2                 : Normal EEPROM is 2Kbit (256b)
    CONFIG_AT24XX_ADDR=0x57              : Normal EEPROM address */
    CONFIG_AT24XX_EXTENDED=y             : Supports an extended memory region
    CONFIG_AT24XX_EXTSIZE=160            : Extended address up to 0x9f

  RTOS Features ->Work Queue Support
    CONFIG_SCHED_WORKQUEUE=y             : Work queue support is needed
    CONFIG_SCHED_HPWORK=y
    CONFIG_SCHED_HPWORKSTACKSIZE=2048    : Might need to be increased

  Application Configuration -> Network Utilities
    CONFIG_NETDB_DNSCLIENT=y             : Enable host address resolution
    CONFIG_NETUTILS_TELNETD=y            : Enable the Telnet daemon
    CONFIG_NETUTILS_TFTPC=y              : Enable TFTP data file transfers for get and put commands
    CONFIG_NETUTILS_NETLIB=y             : Network library support is needed
    CONFIG_NETUTILS_WEBCLIENT=y          : Needed for wget support
                                         : Defaults should be okay for other options
  Application Configuration -> NSH Library
    CONFIG_NSH_TELNET=y                  : Enable NSH session via Telnet
    CONFIG_NSH_IPADDR=0x0a000002         : Select an IP address
    CONFIG_NSH_DRIPADDR=0x0a000001       : IP address of gateway/host PC
    CONFIG_NSH_NETMASK=0xffffff00        : Netmask
    CONFIG_NSH_NOMAC=n                   : We will get the IP address from EEPROM
                                         : Defaults should be okay for other options

Cache-Related Issues
--------------------

I- and D-Caches can be enabled but the D-Cache must be enabled in write-
through mode.  This is to work around issues with the RX and TX descriptors
with are 8-bytes in size.  But the D-Cache cache line size is 32-bytes.
That means that you cannot reload, clean or invalidate a descriptor without
also effecting three neighboring descriptors. Setting write through mode
eliminates the need for cleaning the D-Cache.  If only reloading and
invalidating are done, then there is no problem.

Using the network with NSH
--------------------------

So what can you do with this networking support?  First you see that
NSH has several new network related commands:

  ifconfig, ifdown, ifup:  Commands to help manage your network
  get and put:             TFTP file transfers
  wget:                    HTML file transfers
  ping:                    Check for access to peers on the network
  Telnet console:          You can access the NSH remotely via telnet.

You can also enable other add on features like full FTP or a Web
Server or XML RPC and others.  There are also other features that
you can enable like DHCP client (or server) or network name
resolution.

By default, the IP address of the SAME70-XPLD will be 10.0.0.2 and
it will assume that your host is the gateway and has the IP address
10.0.0.1.

  nsh> ifconfig
  eth0    HWaddr 00:e0:de:ad:be:ef at UP
          IPaddr:10.0.0.2 DRaddr:10.0.0.1 Mask:255.255.255.0

You can use ping to test for connectivity to the host (Careful,
Window firewalls usually block ping-related ICMP traffic).  On the
target side, you can:

  nsh> ping 10.0.0.1
  PING 10.0.0.1 56 bytes of data
  56 bytes from 10.0.0.1: icmp_seq=1 time=0 ms
  56 bytes from 10.0.0.1: icmp_seq=2 time=0 ms
  56 bytes from 10.0.0.1: icmp_seq=3 time=0 ms
  56 bytes from 10.0.0.1: icmp_seq=4 time=0 ms
  56 bytes from 10.0.0.1: icmp_seq=5 time=0 ms
  56 bytes from 10.0.0.1: icmp_seq=6 time=0 ms
  56 bytes from 10.0.0.1: icmp_seq=7 time=0 ms
  56 bytes from 10.0.0.1: icmp_seq=8 time=0 ms
  56 bytes from 10.0.0.1: icmp_seq=9 time=0 ms
  56 bytes from 10.0.0.1: icmp_seq=10 time=0 ms
  10 packets transmitted, 10 received, 0% packet loss, time 10100 ms

NOTE: In this configuration is is normal to have packet loss > 0%
the first time you ping due to the default handling of the ARP
table.

On the host side, you should also be able to ping the SAME70-XPLD:

  $ ping 10.0.0.2

You can also log into the NSH from the host PC like this:

  $ telnet 10.0.0.2
  Trying 10.0.0.2...
  Connected to 10.0.0.2.
  Escape character is '^]'.
  sh_telnetmain: Session [3] Started

  NuttShell (NSH) NuttX-7.9
  nsh> help
  help usage:  help [-v] [<cmd>]

    [           echo        ifconfig    mkdir       mw          sleep
    ?           exec        ifdown      mkfatfs     ping        test
    cat         exit        ifup        mkfifo      ps          umount
    cp          free        kill        mkrd        put         usleep
    cmp         get         losetup     mh          rm          wget
    dd          help        ls          mount       rmdir       xd
    df          hexdump     mb          mv          sh

  Builtin Apps:
  nsh>

NOTE:  If you enable this feature, you experience a delay on booting.
That is because the start-up logic waits for the network connection
to be established before starting NuttX.  In a real application, you
would probably want to do the network bringup on a separate thread
so that access to the NSH prompt is not delayed.

This delay will be especially long if the board is not connected to
a network.  On the order of a minute!  You will probably think that
NuttX has crashed!  And then, when it finally does come up, the
network will not be available.

Network Initialization Thread
-----------------------------
There is a configuration option enabled by CONFIG_NSH_NETINIT_THREAD
that will do the NSH network bring-up asynchronously in parallel on
a separate thread.  This eliminates the (visible) networking delay
altogether.  This networking initialization feature by itself has
some limitations:

  - If no network is connected, the network bring-up will fail and
    the network initialization thread will simply exit.  There are no
    retries and no mechanism to know if the network initialization was
    successful.

  - Furthermore, there is no support for detecting loss of the network
    connection and recovery of networking when the connection is restored.

Both of these shortcomings can be eliminated by enabling the network
monitor:

Network Monitor
---------------
By default the network initialization thread will bring-up the network
then exit, freeing all of the resources that it required.  This is a
good behavior for systems with limited memory.

If the CONFIG_NSH_NETINIT_MONITOR option is selected, however, then the
network initialization thread will persist forever; it will monitor the
network status.  In the event that the network goes down (for example, if
a cable is removed), then the thread will monitor the link status and
attempt to bring the network back up.  In this case the resources
required for network initialization are never released.

Pre-requisites:

  - CONFIG_NSH_NETINIT_THREAD as described above.

  - CONFIG_NETDEV_PHY_IOCTL. Enable PHY IOCTL commands in the Ethernet
    device driver. Special IOCTL commands must be provided by the Ethernet
    driver to support certain PHY operations that will be needed for link
    management. There operations are not complex and are implemented for
    the Atmel SAMV7 family.

  - CONFIG_ARCH_PHY_INTERRUPT. This is not a user selectable option.
    Rather, it is set when you select a board that supports PHY interrupts.
    In most architectures, the PHY interrupt is not associated with the
    Ethernet driver at all. Rather, the PHY interrupt is provided via some
    board-specific GPIO and the board-specific logic must provide support
    for that GPIO interrupt. To do this, the board logic must do two things:
    (1) It must provide the function arch_phy_irq() as described and
    prototyped in the nuttx/include/nuttx/arch.h, and (2) it must select
    CONFIG_ARCH_PHY_INTERRUPT in the board configuration file to advertise
    that it supports arch_phy_irq().  This logic can be found at
    nuttx/configs/sama5d4-ek/src/sam_ethernet.c.

  - And a few other things: UDP support is required (CONFIG_NET_UDP) and
    signals must not be disabled (CONFIG_DISABLE_SIGNALS).

Given those prerequisites, the network monitor can be selected with these
additional settings.

  Networking Support -> Networking Device Support
    CONFIG_NETDEV_PHY_IOCTL=y             : Enable PHY ioctl support

  Application Configuration -> NSH Library -> Networking Configuration
    CONFIG_NSH_NETINIT_THREAD             : Enable the network initialization thread
    CONFIG_NSH_NETINIT_MONITOR=y          : Enable the network monitor
    CONFIG_NSH_NETINIT_RETRYMSEC=2000     : Configure the network monitor as you like
    CONFIG_NSH_NETINIT_SIGNO=18

USBHS Device Controller Driver
==============================
The USBHS device controller driver is enabled with he following configuration
settings:

  Device Drivers -> USB Device Driver Support
    CONFIG_USBDEV=y                           : Enable USB device support
  For full-speed/low-power mode:
    CONFIG_USBDEV_DUALSPEED=n                 : Disable High speed support
  For high-speed/normal mode:
    CONFIG_USBDEV_DUALSPEED=y                 : Enable High speed support
    CONFIG_USBDEV_DMA=y                       : Enable DMA methods
    CONFIG_USBDEV_MAXPOWER=100                : Maximum power consumption
    CONFIG_USBDEV_SELFPOWERED=y               : Self-powered device

  System Type -> SAMV7 Peripheral Selection
    CONFIG_SAMV7_USBDEVHS=y

  System Type -> SAMV7 USB High Sppeed Device Controller (DCD options
  For full-speed/low-power mode:
    CONFIG_SAMV7_USBDEVHS_LOWPOWER=y          : Select low power mode
  For high-speed/normal mode:
    CONFIG_SAMV7_USBDEVHS_LOWPOWER=n          : Don't select low power mode
    CONFIG_SAMV7_USBHS_NDTDS=32               : Number of DMA transfer descriptors
    CONFIG_SAMV7_USBHS_PREALLOCATE=y          : Pre-allocate descriptors

In order to be usable, you must all enabled some class driver(s) for the
USBHS device controller.  Here, for example, is how to configure the CDC/ACM
serial device class:

  Device Drivers -> USB Device Driver Support
    CONFIG_CDCACM=y                           : USB Modem (CDC ACM) support
    CONFIG_CDCACM_EP0MAXPACKET=64             : Enpoint 0 packet size
    CONFIG_CDCACM_EPINTIN=1                   : Interrupt IN endpoint number
    CONFIG_CDCACM_EPINTIN_FSSIZE=64           : Full speed packet size
    CONFIG_CDCACM_EPINTIN_HSSIZE=64           : High speed packet size
    CONFIG_CDCACM_EPBULKOUT=3                 : Bulk OUT endpoint number
    CONFIG_CDCACM_EPBULKOUT_FSSIZE=64         : Full speed packet size
    CONFIG_CDCACM_EPBULKOUT_HSSIZE=512        : High speed packet size
    CONFIG_CDCACM_EPBULKIN=2                  : Bulk IN endpoint number
    CONFIG_CDCACM_EPBULKIN_FSSIZE=64          : Full speed packet size
    CONFIG_CDCACM_EPBULKIN_HSSIZE=512         : High speed packet size
    CONFIG_CDCACM_NWRREQS=4                   : Number of write requests
    CONFIG_CDCACM_NRDREQS=8                   : Number of read requests
    CONFIG_CDCACM_BULKIN_REQLEN=768           : Size of write request buffer
    CONFIG_CDCACM_RXBUFSIZE=256               : Serial read buffer size
    CONFIG_CDCACM_TXBUFSIZE=256               : Serial transmit buffer size
    CONFIG_CDCACM_VENDORID=0x0525             : Vendor ID
    CONFIG_CDCACM_PRODUCTID=0xa4a7            : Product ID
    CONFIG_CDCACM_VENDORSTR="NuttX"           : Vendor string
    CONFIG_CDCACM_PRODUCTSTR="CDC/ACM Serial" : Product string

  Device Drivers -> Serial Driver Support
    CONFIG_SERIAL_REMOVABLE=y                 : Support for removable serial device

The CDC/ACM application provides commands to connect and disconnect the
CDC/ACM serial device:

    CONFIG_SYSTEM_CDCACM=y                     : Enable connect/disconnect support
    CONFIG_SYSTEM_CDCACM_DEVMINOR=0            : Use device /dev/ttyACM0
    CONFIG_CDCACM_RXBUFSIZE=???                : A large RX may be needed

If you include this CDC/ACM application, then you can connect the CDC/ACM
serial device to the host by entering the command 'sercon' and you detach
the serial device with the command 'serdis'.  If you do no use this
application, they you will have to write logic in your board initialization
code to initialize and attach the USB device.

MCAN1 Loopback Test
===================

  MCAN1
  -----
  SAM E70 Xplained has two MCAN modules that performs communication according
  to ISO11898-1 (Bosch CAN specification 2.0 part A,B) and Bosch CAN FD
  specification V1.0.  MCAN1 is connected to an on-board ATA6561 CAN physical-layer
  transceiver.

    ------- -------- -------- -------------
    SAM E70 FUNCTION ATA6561  SHARED
    PIN              FUNCTION FUNCTIONALITY
    ------- -------- -------- -------------
    PC14    CANTX1   TXD      Shield
    PC12    CANRX1   RXD      Shield
    ------- -------- -------- -------------

  Enabling MCAN1
  --------------
  These modifications may be applied to the same70-xplained/nsh configuration in order
  to enable MCAN1:

    Device Drivers -> CAN Driver support
       CONFIG_CAN=y                            # Enable the upper-half CAN driver
       CONFIG_CAN_FIFOSIZE=8
       CONFIG_CAN_NPENDINGRTR=4

    System Type -> SAMV7 Peripheral Selections
       CONFIG_SAMV7_MCAN1=y                    # Enable MCAN1 as the lower-half

    System Type -> MCAN device driver options
       CONFIG_SAMV7_MCAN_CLKSRC_MAIN=y         # Use the MAIN clock as the source
       CONFIG_SAMV7_MCAN_CLKSRC_PRESCALER=1

    System Type ->MCAN device driver options -> MCAN1 device driver options
       CONFIG_SAMV7_MCAN1_ISO11899_1=y         # Loopback test only support ISO11899-1
       CONFIG_SAMV7_MCAN1_LOOPBACK=y           # Needed for loopback test
       CONFIG_SAMV7_MCAN1_BITRATE=500000       # Not critical for loopback test
       CONFIG_SAMV7_MCAN1_PROPSEG=2            # Bit timing setup
       CONFIG_SAMV7_MCAN1_PHASESEG1=11         # " " "    " "   "
       CONFIG_SAMV7_MCAN1_PHASESEG2=11         # " " "    " "   "
       CONFIG_SAMV7_MCAN1_FSJW=4               # " " "    " "   "
       CONFIG_SAMV7_MCAN1_FBITRATE=2000000     # CAN_FD BTW mode is not used
       CONFIG_SAMV7_MCAN1_FPROPSEG=2           # "    " " " "  " "" " " "  "
       CONFIG_SAMV7_MCAN1_FPHASESEG1=4         # "    " " " "  " "" " " "  "
       CONFIG_SAMV7_MCAN1_FPHASESEG2=4         # "    " " " "  " "" " " "  "
       CONFIG_SAMV7_MCAN1_FFSJW=2              # "    " " " "  " "" " " "  "
       CONFIG_SAMV7_MCAN1_NSTDFILTERS=0        # Filters are not used in the loopback test
       CONFIG_SAMV7_MCAN1_NEXTFILTERS=0        # "     " " " " " "  " "" " " "      " "  "
       CONFIG_SAMV7_MCAN1_RXFIFO0_32BYTES=y    # Each RX FIFO0 element is 32 bytes
       CONFIG_SAMV7_MCAN1_RXFIFO0_SIZE=8       # There are 8 queue elements
       CONFIG_SAMV7_MCAN1_RXFIFO0_32BYTES=y    # Each RX FIFO1 element is 32 bytes
       CONFIG_SAMV7_MCAN1_RXFIFO0_SIZE=8       # There are 8 queue elements
       CONFIG_SAMV7_MCAN1_RXBUFFER_32BYTES=y   # Each RX BUFFER is 32 bytes
       CONFIG_SAMV7_MCAN1_TXBUFFER_32BYTES=y   # Each TX BUFFER is 32 bytes
       CONFIG_SAMV7_MCAN1_TXFIFOQ_SIZE=8       # There are 8 queue elements
       CONFIG_SAMV7_MCAN1_TXEVENTFIFO_SIZE=0   # The event FIFO is not used

    Board Selection
       CONFIG_LIB_BOARDCTL=y                   # Needed for CAN initialization
       CONFIG_BOARDCTL_CANINIT=y               # Enabled CAN initialization

    Enabling the CAN Loopback Test
    ------------------------------
    Application Configuration -> Examples -> CAN Example
      CONFIG_EXAMPLES_CAN=y                    # Enables the CAN test

    Enabling CAN Debug Output
    -------------------------
    Build Setup -> Debug Options
      CONFIG_DEBUG=y                           # Enables general debug features
      CONFIG_DEBUG_VERBOSE=y                   # Enables verbose output
      CONFIG_DEBUG_CAN=y                       # Enables debug output from CAN

      CONFIG_STACK_COLORATION=y                # Monitor stack usage
      CONFIG_DEBUG_SYMBOLS=y                   # Needed only for use with a debugger
      CONFIG_DEBUG_NOOPT=y                     # Disables optimization

    System Type -> MCAN device driver options
     CONFIG_SAMV7_MCAN_REGDEBUG=y              # Super low level register debug output

SPI Slave
=========

  An interrutp driven SPI slave driver as added on 2015-08-09 but has not
  been verified as of this writing. See discussion in include/nuttx/spi/slave.h
  and below.

  I do not yet have a design that supports SPI slave DMA.  And, under
  certain, very limited conditions, I think it can be done.  Those
  certain conditions are:

  a) The master does not tie the chip select to ground.  The master must
     raise chip select at the end of the transfer.  Then I do not need to
     know the length of the transfer; I can cancel the DMA when the chip
     is de-selected.

  b) The protocol includes a dummy read after sending the command.  This
     is very common in SPI device and should not be an issue if it is
     specified.   This dummy read time provides time to set up the DMA.
     So the protocol would be:

     i)   Master drops the chip select.
     ii)  Master sends the command which will indicate whether the master
          is reading, writing, or exchanging data.  The master discards
          the garbage return value.
     iii) Slave is interrupted when the command word is received.  The
          SPI device then decodes the command word and setups up the
          subsequent DMA.
     iv)  Master sends a dummy word and discards the return value.
          During the bit times to shift the dummy word, the slave has time
          to set up the DMA.
     v)   Master then reads or writes (or exchanges) the data  If the DMA
          is in place, the transfer should continue normally.
     vi)  At the end of the data transfer the master raises the chip
     select.

   c) There are limitations in the word time, i.e., the time between the
      interrupt for each word shifted in from the master.

  The controller driver will get events after the receipt of each word in
  ii), iv), and v).  The time between each word will be:

    word-time = nbits * bit time + inter-word-gap

  So for an 8 bit interface at 20MHz, the words will be received from the
  master a 8 * 50nsec = 400 nsec + inter-word-gap.  That is the time
  during which the dummy word would be shifted and during which we
  receive the interrupt for the command word, interpret the command word,
  and to set up the DMA for the remaining word transfer.  I don't think
  that is possible, at least not at 20 MHz.

  That is far too fast even for the interrupt driven solution that I have
  in place now.  It could not work at 20MHz.  If we suppose that interrupt
  processing is around 1 usec, then an 8 bit interface could not have bit
  times more than 125 nsec or 8 KHz.  Interrupt handling should be faster
  than 1 usec, but not a lot faster.  I have not benchmarked it.  NuttX
  also supports special, zero latency interrupts that could bring the
  interrupt time down even more.

  Note that we would also have a little more processing time if you used
  16-bit SPI word size.

  Note also that the interrupt driven approach would have this same basic
  performance limitation with the additional disadvantage that:

  a) The driver will receive two interrupts per word exchanged:

     i)  One interrupt will be received when the word is shifted in from
         the master (at the end of 8-bit times).  This is a data received
         interrupt.

     ii) And another interrupt when the next words moved to the shift-out
         register, freeing up the transmit holding register.  This is the
         data sent interrupt.

     The ii) event should be very soon after the i) event.

     Without DMA, the only way to reduce the interrupt rate would be to add
     interrupt-level polling to detect the when transmit holding register
     is available.  That is not really a good idea.

  b) It will hog all of the CPU for the duration of the transfer).

Debugging
=========

  The on-board EDBG appears to work only with Atmel Studio.  You can however,
  simply connect a SAM-ICE or J-Link to the JTAG/SWD connector on the board
  and that works great.  The only tricky thing is getting the correct
  orientation of the JTAG connection.

  I have been using Atmel Studio to write code to flash then I use the Segger
  J-Link GDB server to debug.  I have been using the 'Device Programming' I
  available under the Atmel Studio 'Tool' menu.  I have to disconnect the
  SAM-ICE while programming with the EDBG.  I am sure that you could come up
  with a GDB server-only solution if you wanted.

  I run GDB like this from the directory containing the NuttX ELF file:

    arm-none-eabi-gdb
    (gdb) target remote localhost:2331
    (gdb) mon reset
    (gdb) file nuttx
    (gdb) ... start debugging ...

Configurations
==============

Information Common to All Configurations
----------------------------------------
Each SAME70-XPLD configuration is maintained in a sub-directory and
can be selected as follow:

  cd tools
  ./configure.sh same70-xplained/<subdir>
  cd -
  . ./setenv.sh

Before sourcing the setenv.sh file above, you should examine it and perform
edits as necessary so that TOOLCHAIN_BIN is the correct path to the directory
than holds your toolchain binaries.

And then build NuttX by simply typing the following.  At the conclusion of
the make, the nuttx binary will reside in an ELF file called, simply, nuttx.

  make oldconfig
  make

The <subdir> that is provided above as an argument to the tools/configure.sh
must be is one of the following.

NOTES:

  1. These configurations use the mconf-based configuration tool.  To
    change any of these configurations using that tool, you should:

    a. Build and install the kconfig-mconf tool.  See nuttx/README.txt
       see additional README.txt files in the NuttX tools repository.

    b. Execute 'make menuconfig' in nuttx/ in order to start the
       reconfiguration process.

  2. Unless stated otherwise, all configurations generate console
     output on UART3 (i.e., for the Arduino serial shield).

  3. All of these configurations are set up to build under Windows using the
     "GNU Tools for ARM Embedded Processors" that is maintained by ARM
     (unless stated otherwise in the description of the configuration).

       https://launchpad.net/gcc-arm-embedded

     As of this writing (2015-03-11), full support is difficult to find
     for the Cortex-M7, but is supported by at least this realeasse of
     the ARM GNU tools:

       https://launchpadlibrarian.net/192228215/release.txt

     Current (2105-07-31) setenv.sh file are configured to use this
     release:

       https://launchpadlibrarian.net/209776344/release.txt

     That toolchain selection can easily be reconfigured using
     'make menuconfig'.  Here are the relevant current settings:

     Build Setup:
       CONFIG_HOST_WINDOWS=y               : Window environment
       CONFIG_WINDOWS_CYGWIN=y             : Cywin under Windows

     System Type -> Toolchain:
       CONFIG_ARMV7M_TOOLCHAIN_GNU_EABIW=y : GNU ARM EABI toolchain

     NOTE: As of this writing, there are issues with using this tool at
     the -Os level of optimization.  This has not been proven to be a
     compiler issue (as least not one that might not be fixed with a
     well placed volatile qualifier).  However, in any event, it is
     recommend that you use not more that -O2 optimization.

Configuration sub-directories
-----------------------------

  knsh:

    This is identical to the nsh configuration below except that NuttX
    is built as a kernel-mode, monolithic module and the user applications
    are built separately.  There are three very similar NSH configurations:

      - knsh.  This is a somewhat simplified version of the nsh configuration
        that builds using the protected build mode (CONFIG_BUILD_PROTECTED=y).
      - nsh.  This configuration is focused on low level, command-line
        driver testing.  It has no network.
      - netnsh.  This configuration is focused on network testing and
        has only limited command support.

    It is recommends to use a special make command; not just 'make' but make
    with the following two arguments:

        make pass1 pass2

    In the normal case (just 'make'), make will attempt to build both user-
    and kernel-mode blobs more or less interleaved.  This actual works!
    However, for me it is very confusing so I prefer the above make command:
    Make the user-space binaries first (pass1), then make the kernel-space
    binaries (pass2)

    NOTES:

    1. At the end of the build, there will be several files in the top-level
       NuttX build directory:

       PASS1:
         nuttx_user.elf    - The pass1 user-space ELF file
         nuttx_user.hex    - The pass1 Intel HEX format file (selected in defconfig)
         User.map          - Symbols in the user-space ELF file

       PASS2:
         nuttx             - The pass2 kernel-space ELF file
         nuttx.hex         - The pass2 Intel HEX file (selected in defconfig)
         System.map        - Symbols in the kernel-space ELF file

       The J-Link programmer will except files in .hex, .mot, .srec, and .bin
       formats.

    2. Combining .hex files.  If you plan to use the .hex files with your
       debugger or FLASH utility, then you may need to combine the two hex
       files into a single .hex file.  Here is how you can do that.

       a. The 'tail' of the nuttx.hex file should look something like this
          (with my comments added):

            $ tail nuttx.hex
            # 00, data records
            ...
            :10 9DC0 00 01000000000800006400020100001F0004
            :10 9DD0 00 3B005A0078009700B500D400F300110151
            :08 9DE0 00 30014E016D0100008D
            # 05, Start Linear Address Record
            :04 0000 05 0800 0419 D2
            # 01, End Of File record
            :00 0000 01 FF

          Use an editor such as vi to remove the 05 and 01 records.

       b. The 'head' of the nuttx_user.hex file should look something like
          this (again with my comments added):

            $ head nuttx_user.hex
            # 04, Extended Linear Address Record
            :02 0000 04 0801 F1
            # 00, data records
            :10 8000 00 BD89 01084C800108C8110208D01102087E
            :10 8010 00 0010 00201C1000201C1000203C16002026
            :10 8020 00 4D80 01085D80010869800108ED83010829
            ...

          Nothing needs to be done here.  The nuttx_user.hex file should
          be fine.

       c. Combine the edited nuttx.hex and un-edited nuttx_user.hex
          file to produce a single combined hex file:

          $ cat nuttx.hex nuttx_user.hex >combined.hex

       Then use the combined.hex file with the to write the FLASH image.
       If you do this a lot, you will probably want to invest a little time
       to develop a tool to automate these steps.

  netnsh:

    Configures the NuttShell (nsh) located at examples/nsh.  There are three
    very similar NSH configurations:

      - knsh.  This is a somewhat simplified version of the nsh configuration
        that builds using the protected build mode (CONFIG_BUILD_PROTECTED=y).
      - nsh.  This configuration is focused on low level, command-line
        driver testing.  It has no network.
      - netnsh.  This configuration is focused on network testing and
        has only limited command support.

    NOTES:

    1. The serial console is configured by default for use with and Arduino
       serial shield (UART3).  You will need to reconfigure if you will
       to use a different U[S]ART.

    2. Default stack sizes are large and should really be tuned to reduce
       the RAM footprint:

         CONFIG_SCHED_HPWORKSTACKSIZE=2048
         CONFIG_IDLETHREAD_STACKSIZE=1024
         CONFIG_USERMAIN_STACKSIZE=2048
         CONFIG_PTHREAD_STACK_MIN=256
         CONFIG_PTHREAD_STACK_DEFAULT=2048
         CONFIG_POSIX_SPAWN_PROXY_STACKSIZE=1024
         CONFIG_TASK_SPAWN_DEFAULT_STACKSIZE=2048
         CONFIG_BUILTIN_PROXY_STACKSIZE=1024
         CONFIG_NSH_TELNETD_DAEMONSTACKSIZE=2048
         CONFIG_NSH_TELNETD_CLIENTSTACKSIZE=2048

    3. NSH built-in applications are supported.  There are, however, not
       enabled built-in applications.

       Binary Formats:
         CONFIG_BUILTIN=y           : Enable support for built-in programs

       Application Configuration:
         CONFIG_NSH_BUILTIN_APPS=y  : Enable starting apps from NSH command line

    4. The network initialization thread and the NSH network montior are
       enabled in this configuration. As a result, networking initialization
       is performed asynchronously with NSH bring-up.  For more information,
       see the paragraphs above entitled "Network Initialization Thread" and
       "Network Monitor".

    5. SDRAM is NOT enabled in this configuration.

    6. TWI/I2C

       TWIHS0 is enabled in this configuration.  The SAM E70 Xplained
       supports one devices on the one on-board I2C device on the TWIHS0 bus:
       The AT24MAC402 serial EEPROM described above.
       Relevant configuration settings:

         CONFIG_SAMV7_TWIHS0=y
         CONFIG_SAMV7_TWIHS0_FREQUENCY=100000

         CONFIG_I2C=y
         CONFIG_I2C_TRANSFER=y

    7. TWIHS0 is used to support 256 byte non-volatile storage.  This EEPROM
       holds the assigned MAC address which is necessary for networking. The
       EEPROM is also available for storage of configuration data using the
       MTD configuration as described above under the heading, "MTD
       Configuration Data".

    8. Support for HSMCI is built-in by default. The SAME70-XPLD provides
       one full-size SD memory card slot.  Refer to the section entitled
       "SD card" for configuration-related information.

       See "Open Issues" above for issues related to HSMCI.

       The auto-mounter is not enabled.  See the section above entitled
       "Auto-Mounter".

    9. Performance-related Configuration settings:

       CONFIG_ARMV7M_ICACHE=y                : Instruction cache is enabled
       CONFIG_ARMV7M_DCACHE=y                : Data cache is enabled
       CONFIG_ARMV7M_DCACHE_WRITETHROUGH=y   : Write through mode
       CONFIG_ARCH_FPU=y                     : H/W floating point support is enabled
       CONFIG_ARCH_DPFPU=y                   : 64-bit H/W floating point support is enabled

       # CONFIG_ARMV7M_ITCM is not set       : Support not yet in place
       # CONFIG_ARMV7M_DTCM is not set       : Support not yet in place

       I- and D-Caches are enabled but the D-Cache must be enabled in write-
       through mode.  This is to work around issues with the RX and TX
       descriptors with are 8-bytes in size.  But the D-Cache cache line
       size is 32-bytes.  That means that you cannot reload, clean or
       invalidate a descriptor without also effecting three neighboring
       descriptors. Setting write through mode eliminates the need for
       cleaning the D-Cache.  If only reloading and invalidating are done,
       then there is no problem.

       Stack sizes are also large to simplify the bring-up and should be
       tuned for better memory usages.

    STATUS:
    2015-03-29:  I- and D-caches are currently enabled, but as noted
      above, the D-Cache must be enabled in write-through mode.  Also -Os
      optimization is not being used (-O2).  If the cache is enabled in
      Write-Back mode or if higher levels of optimization are enabled, then
      there are failures when trying to ping the target from a host.

  nsh:

    Configures the NuttShell (nsh) located at examples/nsh.  There are three
    very similar NSH configurations:

      - knsh.  This is a somewhat simplified version of the nsh configuration
        that builds using the protected build mode (CONFIG_BUILD_PROTECTED=y).
      - nsh.  This configuration is focused on low level, command-line
        driver testing.  It has no network.
      - netnsh.  This configuration is focused on network testing and
        has only limited command support.

    NOTES:

    1. The serial console is configured by default for use with the EDBG VCOM
       (USART1).  You will need to reconfigure if you will to use a different
       U[S]ART.

    2. Default stack sizes are large and should really be tuned to reduce
       the RAM footprint:

         CONFIG_ARCH_INTERRUPTSTACK=2048
         CONFIG_IDLETHREAD_STACKSIZE=1024
         CONFIG_USERMAIN_STACKSIZE=2048
         CONFIG_PTHREAD_STACK_DEFAULT=2048
         ... and others ...

    3. NSH built-in applications are supported.

       Binary Formats:
         CONFIG_BUILTIN=y           : Enable support for built-in programs

       Application Configuration:
         CONFIG_NSH_BUILTIN_APPS=y  : Enable starting apps from NSH command line

    4. SDRAM is enabled in this configuration.  Here are the relevant
       configuration settings:

       System Type
         CONFIG_SAMV7_SDRAMC=y
         CONFIG_SAMV7_SDRAMSIZE=2097152

       SDRAM is not added to the heap in this configuration.  To do that
       you would need to set CONFIG_SAMV7_SDRAMHEAP=y and CONFIG_MM_REGIONS=2.
       Instead, the SDRAM is set up so that is can be used with a destructive
       RAM test enabled with this option:

       Application Configuration:
         CONFIG_SYSTEM_RAMTEST=y

       The RAM test can be executed as follows:

         nsh> ramtest -w 70000000 2097152

         NuttShell (NSH) NuttX-7.8
         nsh> ramtest -w 70000000 2097152
         RAMTest: Marching ones: 70000000 2097152
         RAMTest: Marching zeroes: 70000000 2097152
         RAMTest: Pattern test: 70000000 2097152 55555555 aaaaaaaa
         RAMTest: Pattern test: 70000000 2097152 66666666 99999999
         RAMTest: Pattern test: 70000000 2097152 33333333 cccccccc
         RAMTest: Address-in-address test: 70000000 2097152
         nsh>

    5. The button test at apps/examples/buttons is included in the
       configuration.  This configuration illustrates (1) use of the buttons
       on the evaluation board, and (2) the use of PIO interrupts.  Example
       usage:

       NuttShell (NSH) NuttX-7.8
       nsh> help
       help usage:  help [-v] [<cmd>]
       ...
       Builtin Apps:
         buttons
       nsh> buttons 3
       maxbuttons: 3
       Attached handler at 4078f7 to button 0 [SW0], oldhandler:0
       Attached handler at 4078e9 to button 1 [SW1], oldhandler:0
       IRQ:125 Button 1:SW1 SET:00:
         SW1 released
       IRQ:125 Button 1:SW1 SET:02:
         SW1 depressed
       IRQ:125 Button 1:SW1 SET:00:
         SW1 released
       IRQ:90 Button 0:SW0 SET:01:
         SW0 depressed
       IRQ:90 Button 0:SW0 SET:00:
         SW0 released
       IRQ:125 Button 1:SW1 SET:02:
         SW1 depressed
       nsh>

    6. TWI/I2C

       TWIHS0 is enabled in this configuration.  The SAM E70 Xplained
       supports one device on the one on-board I2C device on the TWIHS0 bus:
       The AT24MAC402 serial EEPROM described above.

       In this configuration, the I2C tool at apps/system/i2ctool is
       enabled.  This tools supports interactive access to I2C devices on
       the enabled TWIHS bus.  Relevant configuration settings:

         CONFIG_SAMV7_TWIHS0=y
         CONFIG_SAMV7_TWIHS0_FREQUENCY=100000

         CONFIG_I2C=y
         CONFIG_I2C_TRANSFER=y

         CONFIG_SYSTEM_I2CTOOL=y
         CONFIG_I2CTOOL_MINBUS=0
         CONFIG_I2CTOOL_MAXBUS=0
         CONFIG_I2CTOOL_MINADDR=0x03
         CONFIG_I2CTOOL_MAXADDR=0x77
         CONFIG_I2CTOOL_MAXREGADDR=0xff
         CONFIG_I2CTOOL_DEFFREQ=400000

       Example usage:

         nsh> i2c
         Usage: i2c <cmd> [arguments]
         Where <cmd> is one of:

           Show help     : ?
           List busses   : bus
           List devices  : dev [OPTIONS] <first> <last>
           Read register : get [OPTIONS] [<repititions>]
           Show help     : help
           Write register: set [OPTIONS] <value> [<repititions>]
           Verify access : verf [OPTIONS] [<value>] [<repititions>]

         Where common "sticky" OPTIONS include:
           [-a addr] is the I2C device address (hex).  Default: 03 Current: 03
           [-b bus] is the I2C bus number (decimal).  Default: 0 Current: 0
           [-r regaddr] is the I2C device register address (hex).  Default: 00 Current: 00
           [-w width] is the data width (8 or 16 decimal).  Default: 8 Current: 8
           [-s|n], send/don't send start between command and data.  Default: -n Current: -n
           [-i|j], Auto increment|don't increment regaddr on repititions.  Default: NO Current: NO
           [-f freq] I2C frequency.  Default: 400000 Current: 400000

         NOTES:
         o An environment variable like $PATH may be used for any argument.
         o Arguments are "sticky".  For example, once the I2C address is
           specified, that address will be re-used until it is changed.

         WARNING:
         o The I2C dev command may have bad side effects on your I2C devices.
           Use only at your own risk.
         nsh> i2c bus
          BUS   EXISTS?
         Bus 0: YES
         nsh> i2c dev 3 77
              0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
         00:          -- -- -- -- -- -- -- -- -- -- -- -- --
         10: -- -- -- -- -- -- -- -- -- -- 1a -- -- -- -- --
         20: -- -- -- -- -- -- -- -- 28 -- -- -- -- -- -- --
         30: -- -- -- -- -- -- -- 37 -- -- -- -- -- -- -- --
         40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- 4e --
         50: -- -- -- -- -- -- -- 57 -- -- -- -- -- -- -- 5f
         60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
         70: -- -- -- -- -- -- -- --
         nsh>

       Where 0x28 is the address of TWI interface to the EDBG, 0x4e is the
       address of the CP2100CP programmable PLL, and 0x57 and 0x5f are thei
       addresses of the AT2 EEPROM (I am not sure what the other address,i
       0x37, is as this writing).

    7. TWIHS0 is also used to support 256 byte non-volatile storage for
       configuration data using the MTD configuration as described above
       under the heading, "MTD Configuration Data".

    8. Support for HSMCI is built-in by default. The SAME70-XPLD provides
       one full-size SD memory card slot.  Refer to the section entitled
       "SD card" for configuration-related information.

       See "Open Issues" above for issues related to HSMCI.

       The auto-mounter is not enabled.  See the section above entitled
       "Auto-Mounter".

    9. Performance-related Configuration settings:

       CONFIG_ARMV7M_ICACHE=y                : Instruction cache is enabled
       CONFIG_ARMV7M_DCACHE=y                : Data cache is enabled
       CONFIG_ARMV7M_DCACHE_WRITETHROUGH=n   : Write back mode
       CONFIG_ARCH_FPU=y                     : H/W floating point support is enabled
       CONFIG_ARCH_DPFPU=y                   : 64-bit H/W floating point support is enabled

       # CONFIG_ARMV7M_ITCM is not set       : Support not yet in place
       # CONFIG_ARMV7M_DTCM is not set       : Support not yet in place

       Stack sizes are also large to simplify the bring-up and should be
       tuned for better memory usages.

    STATUS:
    2015-03-28: HSMCI TX DMA is disabled.  There are some issues with the TX
      DMA that need to be corrected.

